<?php
    class Message {
        protected $text = "A simple message"; // משתנה של מחרוזת
        public static $count = 0;           // משתנה סטטי
        public function show(){
            echo "<p>$this->text</p>";      //  thisפונקצייה בתוך המחלקה שמדפיסה את המשתנה שגם נמצא בתוך המחלקה בעזרת  
        }
        function __construct($text = ""){    // פנקציית בנאי עם ארגומנט של מחרוזת
            ++self::$count;                  // בעזרת סלף נקרא למשתנה סטטי, ונעלה את הקאונט באחד סופר את כמות האובייקטים שעשינו
            if($text !=""){                  // אם הארגומנט שונה ממחרוזת ריקה
                $this->text = $text;         // אז המשתנה טקסט יהפוך להיות למה שהזינו בארגומנט
            }
        }
    }



class redMessage extends Message{                     // נירש מהמחלקה למעלה
  public function show(){
      echo "<p style = 'color:red'>$this->text</p>";              //נשנה רק את הפונקציית שואו נדפיס את הטקסט בצבע אדום
  }
}

class coloredMessage extends Message {
    protected $color = 'red';                      //ערך ברירת מחדל אדום למשתנה
    public function __set($property,$value){                    // שימוש בפונקציית קסם- פונקציית סט מחייבת שתי ארגומנטים - פרופרטי ווליו
        if($property == 'color'){                           // הפרופרטי שלנו זה קולור
            $colors = array('red','yellow','green');       // מערך של צבעים חוקיים שהגדרנו
            if(in_array($value, $colors)){                // אם הווליו שנגדיר בקובץ האינדקס יהיה אחד מהצבעים שבמערך
                $this->color = $value;                     // לא ניתן לסביבה החיצונית להקליד צבע לא חוקי שלא נמצא במערך - המשתנה קולור יהיה הווליו
            } 
        }
    }
    public function show(){
        echo "<p style = 'color:$this->color'>$this->text</p>";  // על מנת להדפיס לפלט
    }   
}


function showObject($object){           // סתם פונצקיה שלא קשורה לאף מחלקה
    $object->show();                    // לכל מה שיצרנו עד עכשיו יש את הפונקציה שואו
}


?> 