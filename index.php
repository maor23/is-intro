<?php
    include "class.php"; // חיבור של הקובץ קלאס לקובץ הנוכחי
    include "db.php";
    include "query.php";
?>
<html>
    <head>
        <title>Object Oriented PHP</title>  
    </head>
    <body>
        <p>
        <?php
        /*
            $text = 'Hello World';      // הגדרת משתנה
            echo "$text And The Universe"; // שרשור מחרוזות
            echo '<br>';                 // ירידת שורה
            $msg = new Message();        // למחלקה ללא ארגומנט instance הגדרת
            echo '<br>';
            echo Message::$count;        // יצרנו עד כה mesaage מדפיס כמה אוביקטים מסוג
            //echo $msg->text;
            $msg->show();                //  בעזרת האינסטנס פונים לפונקציה שואו שבמחלקה ללא ארגומנט וכך נוכל לראות את מה שרשמנו בפלט כי פונקציית שואו מדפיסה
            $msg1 = new Message("A new text"); // מגדירים אובייקט חדש למחלקה עם ארגומנט
            $msg1->show();                     // בעזרת פונקציית השואו נוכל לראות בפלט את הארגומנט החדש שהדפסנו
            echo '<br>';
            echo Message::$count;
            $msg2 = new Message();
            $msg2->show();
            echo '<br>';
            echo Message::$count;
            echo '<br>';
            $msg3 = new redMessage('a red message');  //יצירת אובייקט חדש עם ארגומנט למחלקה השנייה שלנו
            $msg3 -> show();                          // הפונקציה שואו מדפיסה לפלט כך נראה שזה יופיע בפלט             
            echo '<br>';
            $msg4 = new coloredMessage ('A colored message');   // אינסטנס/אובייקט חדש למחלקה השלישית שלנו
            $msg4 -> color = 'green';                 // נכניס לפרופרטי קולור שהגדרנו בפונקציית סט במחלקה את הווליו ירוק
            //$msg4 -> setColor('red');                 ניתן גם ככה לרשום אבל זאת כתיבה פחות יפה לקוד
            $msg4 -> show();                        // כדי שידפיס ונראה פלט
            echo '<br>';
            $msg4 -> color = 'yellow'; 
            $msg4 -> show(); 
            echo '<br>';
            $msg4 -> color = 'black';    // אין צבע שחור במערך שלנו אז זה ידפיס את הצבע האחרון שהדפסנו 
            $msg4 -> show();             // ידפיס את הצבע האחרון ולא את הברירת מחדל אדום כי לא יצרנו אובייקט חשד
            echo '<br>';
            $msg5 = new coloredMessage ('A colored message'); 
            $msg5 -> color = 'black';              // לאחר שהגדרנו אובייקט חדש זה ידפיס את צבע הברירת מחדל אדום כי אין צבע שחור
            $msg5 -> show();
            echo '<br>';
            showObject($msg5);      // פלומורפיזם
            echo '<br>';
            showObject($msg1);
            echo '<br>';
            */


            // database connection
            $db = new DB('localhost','intro','root',''); // ניצור אובייקט למחלקת מסד הנתונים
            $dbc = $db->connect(); //רק שמריצים את הפונקצייה הזאת רואים האם המסד נתונים התחבר כראוי
            
            $query = new query($dbc); //אובייקט למחלקה קוואיירי, דיביסי זה הארגומנט של הקונסטראקט
            $q = "SELECT * FROM users"; // נגדיר מתשנה לשאילתא
            $result = $query->query($q); // יוצרים משתנה שקורא לאובייקש שמפעיל את הפונקצייה שבאובייקט
            echo '<br>';
            //echo $result->num_rows; //מדפיס את מספר השורות של השאילתא - לצורך בדיקה
            if($result->num_rows >0){ // אם מספר השורות גדול מאפס
                echo '<table>';       // נדפיס טבלה של אייטימאל - כדי שהתשובה תחזור כטבלה
                echo '<tr><th>Name</th><th>Email</th></tr>';
                while($row = $result->fetch_assoc()){      // ניצור לולאה שתיצור טבלה דינאמית
                    echo '<tr>';
                    echo '<td>'.$row['name'].'</td><td>'.$row['email'].'</td>';
                    echo '</tr>';
                }
                echo '</table>';
            }   else{
                echo "Sorry no results";
            }    
        
        ?>  
        </P>
    </body>
</html>